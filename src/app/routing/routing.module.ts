import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NoPreloading, Route, RouterModule} from '@angular/router';
import { DepartmentlistComponent } from '../departmentlist/departmentlist.component';
import { EmployeelistComponent } from '../employeelist/employeelist.component';
import { AboutusComponent } from '../aboutus/aboutus.component';
import { ContactusComponent } from '../contactus/contactus.component';
import { NopagefoundComponent } from '../nopagefound/nopagefound.component';
import { DepartmentdetailsComponent } from '../departmentdetails/departmentdetails.component';
import { OverviewComponent } from '../overview/overview.component';
import { AdddepartmentComponent } from '../adddepartment/adddepartment.component';
import { DeletedepartmentComponent } from '../deletedepartment/deletedepartment.component';

const routes : Route[]=
[
  {path:'', component: EmployeelistComponent},
  {path:'department',component:DepartmentlistComponent },
  {path:'departmentdetails/:id',component:DepartmentdetailsComponent,
children:
[
  {path:'overview', component:OverviewComponent},
  {path:'adddepartment', component:AdddepartmentComponent},
  {path:'deletedepartment', component:DeletedepartmentComponent}
]},

  {path:'employee', component: EmployeelistComponent},
  {path:'aboutus', component:AboutusComponent},
  {path:'contactus', component:ContactusComponent},
  {path:'**' , component: NopagefoundComponent}

]


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)

  ],
  exports: [RouterModule]
})
export class RoutingModule { }

export const components = [DepartmentlistComponent, EmployeelistComponent,
ContactusComponent, AboutusComponent, NopagefoundComponent, DepartmentdetailsComponent
, AdddepartmentComponent, DeletedepartmentComponent]
