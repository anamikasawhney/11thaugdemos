import { Component } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-departmentlist',
  template: `<h1> List of Departments </h1>
  <ul class="items" *ngFor="let department of departments">
  <li (click)="selectvalue(department.id)" > <span class="primary"> {{department.id}} </span>  -- {{department.name}} </li> `,
  styleUrls: ['./departmentlist.component.css']
})
export class DepartmentlistComponent {
  constructor(private _router : Router){}
departments =[
  {id:1, "name":"Angular"},
  {id:2, "name":"C#"},
  {id:3, "name":"Java"},
  {id:4, "name":"C++"},
  {id:5, "name":"Spring"},

]
selectvalue(departmentid : number)
{
  console.log("Vlaue is selected"  + departmentid);
  this._router.navigate(['departmentdetails/'+departmentid])

}
}
